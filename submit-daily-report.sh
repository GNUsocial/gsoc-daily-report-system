#!/bin/sh
# Change the next two variables to the correct values
start_month_offset=5; # Things started in May
nickname="$(git config user.name)"

month="$(date --date="-$start_month_offset months" +%-m)"
dir="$nickname/$month/"
nday="$(date --date="-1 day" +%-d)"
file="$dir/$nday"

mkdir -p "$dir"

if [ ! -e "$file" ]
then
        cat > "$file" <<EOF
{
  "summary": "",
  "dedicated_hours":
}

EOF
        commit_message="[$nickname] added daily report $nday of month $month"
else
        commit_message="[$nickname] updated daily report $nday of month $month"
fi

$EDITOR "$file"

git pull
git add "$file"
git commit -m "$commit_message"
git push
